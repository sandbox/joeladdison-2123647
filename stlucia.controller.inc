<?php

/**
 * @file
 * Provides a controller building upon the core controller.
 * Does not support revisions of content.
 */

/**
 * Controller class for St Lucia entities.
 *
 * This class extends the DrupalDefaultEntityController class, overriding the
 * 'load' method for entities based on RDF types.
 */
class StLuciaController extends EntityAPIControllerExportable implements EntityAPIControllerInterface {

  public function load($ids = array(), $conditions = array()) {
    // Get cached entities.
    $entities = $this->cacheGet($ids, $conditions);

    // Load un-cached entities.
    if (($ids === FALSE) || ($load_ids = array_diff($ids, array_keys($entities)))) {
      $bundle = $this->entityInfo['bundles'][$this->entityType];
      $endpoint = sparql_registry_load_by_uri($bundle['endpoint_uri']);
      $uri_format = $bundle['uri_format'];

      $results = array();

      if (easyrdf()) {
        foreach ($load_ids as $id) {
          // Build query.
          $query = $this->buildQuery(array($id), $conditions);
          $ep = new EasyRdf_Sparql_Client($bundle['endpoint_uri']);
          $result = $ep->query($query);

          // Process the result data.
          $data = array('type' => $this->entityType);
          foreach ($bundle['rdf_mapping'] as $field_name => $field) {
            if ($field_name == "rdftype") {
              continue;
            }

            // Extract the result value, based on the type.
            $value = NULL;
            $field_result = $result[0]->{$field_name};
            if ($field_result instanceof EasyRdf_Resource) {
              $value = $field_result->getUri();
            }
            else {
              $value = $field_result->getValue();
            }

            // Build the field, with no language
            $data[$field_name] = array(
              LANGUAGE_NONE => array(
                0 => array(
                  'value' => $value
                )
              )
            );
          }
          $data['id'] = $id;
          $results[$id] = (object) $data;
        }
      }

      // Cache the results.
      $this->cacheSet($results);

      // Retrieve entities from the cache.
      $entities = $this->cacheGet($ids, $conditions);
    }

    return $entities;
  }

  protected function buildQuery($ids, $conditions = array(), $revision_id = FALSE) {
    $graph_pattern = '';
    $ns = rdf_get_namespaces();
    $required_namespaces = array();
    $prefixes = '';
    $statements = array();

    $bundle = $this->entityInfo['bundles'][$this->entityType];

    // Build select query.
    $select = 'SELECT';

    // Add fields for the entity.
    foreach ($bundle['rdf_mapping'] as $field_name => $field) {
      // If this is the rdftype part of the RDF mapping, skip it.
      if($field_name == 'rdftype') {
        continue;
      }

      $select .= " ?$field_name";

      $p = $field['predicates'][0];
      if (!preg_match('/^http/', $p)) {
        $split = explode(':', $p);
        if(count($split) == 2) {
          $required_namespaces[] = $split[0];
        }
      }

      $o = $field_name;
      $statements[] = " $p ?$o";
    }

    // Filter on identifier
    $graph_pattern = '<' . $this->_id_to_uri($ids[0]) . '> a ' . $bundle['rdf_mapping']['rdftype'][0] . ';';

    // Build the graph pattern.
    if (!empty($statements)) {
      $graph_pattern .= ' ' . implode(';', $statements) . '.';
    }

    $required_namespaces = array_unique($required_namespaces);

    foreach($required_namespaces as $prefix) {
      if (isset($ns[$prefix])) {
        $namespace = $ns[$prefix];
        $prefixes .= "PREFIX $prefix: <$namespace>\n";
      }
    }

    $query = $prefixes;
    $query .= $select . "\n";
    $query .= "WHERE {\n$graph_pattern\n} \n";
    return $query;
  }

  protected function _id_to_uri($id) {
    $bundle = $this->entityInfo['bundles'][$this->entityType];
    $uri = $bundle['uri_format'];
    return str_replace('%', $id, $uri);
  }
}

<?php

/**
 * Get a list of Sarnia entity configurations without using entity APIs.
 * @see stlucia_entity_info()
 *
 * In most cases, field_info_bundles() is preferred, since it is part of Drupal
 * core APIs.
 * @see stlucia_entity_type_load()
 */
function _stlucia_entity_types($reset = FALSE) {
  $types = &drupal_static(__FUNCTION__);

  if (!isset($types) || $reset) {
    // Get information about Sarnia entity types from the database.
    $types = array();
    $types = db_query("SELECT label, machine_name, endpoint_uri, uri_format, description FROM {stlucia_entity_type}")
      ->fetchAllAssoc('machine_name', PDO::FETCH_ASSOC);
  }

  return $types;
}

/**
 * Create or update a St Lucia entity type.
 */
function stlucia_entity_type_save($entity_type) {
  $result = db_merge('stlucia_entity_type')
    ->key(array('machine_name' => $entity_type['machine_name']))
    ->fields(array(
      'label' => $entity_type['label'],
      'endpoint_uri' => $entity_type['endpoint_uri'],
      'uri_format' => $entity_type['uri_format'],
      'description' => $entity_type['description'],
    ))
    ->execute();

  // Reset the array returned by _stlucia_entity_types().
  drupal_static_reset('_stlucia_entity_types');

  return $result;
}

/**
 * Delete a Sarnia entity type.
 */
function stlucia_entity_type_delete($machine_name) {
  // $entity_type = stlucia_entity_type_load($machine_name);

  // Delete any Field API fields on the entity type.
  field_attach_delete_bundle($machine_name, $machine_name);

  // Delete our record of the entity type.
  db_query("DELETE FROM {stlucia_entity_type} WHERE machine_name = :machine_name", array(':machine_name' => $machine_name));

  // Reset the array returned by _stlucia_entity_types().
  drupal_static_reset('_stlucia_entity_types');
}

/**
 * This form appears on a "Sarnia" tab on Search API server configuration pages.
 * It allows administrators to "enable sarnia" for a particular server; when
 * Sarnia is enabled for a Search API server, a Sarnia entity type is registered
 * and a Search API index is created.
 *
 * @see _stlucia_entity_manage_form_access(), stlucia_entity_manage_form_submit(), stlucia_entity_type_save(), search_api_index_insert()
 */
function stlucia_entity_manage_form($form, &$form_state, $entity_type=NULL) {
  $entity_type = (object) $entity_type;
  $form['#entity_type'] = isset($entity_type->machine_name) ? $entity_type->machine_name : '';

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($entity_type->label) ? $entity_type->label : '',
    '#description' => t('The human-readable label of this entity type.'),
    '#required' => TRUE,
  );

  // Machine-readable type name.
  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($entity_type->machine_name) ? $entity_type->machine_name : '',
    '#machine_name' => array(
      'exists' => 'stlucia_entity_type_load',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this resource type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#default_value' => isset($entity_type->description) ? $entity_type->description : '',
    '#description' => t('Describe this entity type. The text will be displayed on the <em>St Lucia Overview</em> page.'),
    '#required' => TRUE,
  );

  $endpoints = sparql_registry_load_multiple();
  $options = array();
  foreach ($endpoints as $endpoint) {
    $options[$endpoint->uri] = $endpoint->title . ': <span class="description">' . $endpoint->uri . '</span>';
  }
  $form['endpoint_uri'] = array(
    '#title' => t('Endpoint this resource can be found in'),
    '#type' => 'radios',
    '#default_value' => isset($entity_type->endpoint_uri) ? $entity_type->endpoint_uri : '',
    '#description' => t('Different endpoints often have different ways of describing similar things. Select the endpoint that this resource can be used with.'),
    '#options' => $options,
    '#required' => TRUE,
  );

  $form['uri_format'] = array(
    '#title' => t('URI Format'),
    '#type' => 'textfield',
    '#default_value' => isset($entity_type->uri_format) ? $entity_type->uri_format : '',
    '#required' => TRUE,
    '#description' => t('Enter the format of the entity type URI, using a <em>%</em> as wildcard for identifier. For example: http://rota.eait.uq.edu.au/resource/building/%'),
    '#required' => TRUE,
  );


  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save resource type'),
    '#weight' => 5,
  );

  if (isset($entity_type->machine_name)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('stlucia_entity_manage_form_delete'),
    );
  }

  return $form;
}

/**
 * Submit function. Creates a Sarnia entity type for a Search API server.
 * @see stlucia_entity_manage_form()
 */
function stlucia_entity_manage_form_submit($form, &$form_state) {
  $label = $form_state['values']['label'];
  // Create a St Lucia entity type.
  $entity_type = array(
    'label' => $label,
    'machine_name' => $form_state['values']['name'],
    'endpoint_uri' => $form_state['values']['endpoint_uri'],
    'uri_format' => $form_state['values']['uri_format'],
    'description' => $form_state['values']['description'],
  );
  $result = stlucia_entity_type_save($entity_type);

  // Rebuild the entity info cache and the menus, so that tabs provided by the
  // Field UI module show up immediately.
  entity_info_cache_clear();
  menu_rebuild();

  // Tell the user what just happened.
  if ($result === MergeQuery::STATUS_INSERT) {
    drupal_set_message(t('Created a St Lucia entity type - %entity_type.', array('%entity_type' => $label)));
  }
  elseif ($result === MergeQuery::STATUS_UPDATE) {
    drupal_set_message(t('Updated St Lucia entity type - %entity_type.', array('%entity_type' => $label)));
  }
}

/**
 * Button submit function: handle the 'Delete' button on the St Lucia
 * entity type management form
 */
function stlucia_entity_manage_form_delete($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $entity_type_machine_name = $form['#entity_type'];
  $form_state['redirect'] = array(
    'admin/structure/stlucia/manage/' . $entity_type_machine_name . '/delete',
    array('query' => $destination)
  );
}

function stlucia_entity_delete_form($form, &$form_state, $entity_type) {
  $entity_type = (object) $entity_type;
  $form['entity_type'] = array(
    '#type' => 'value',
    '#value' => $entity_type,
  );

  return confirm_form($form,
    t('Delete St Lucia entity type %entity_label', array('%entity_label' => $entity_type->label)),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/structure/stlucia/manage/' . $entity_type->machine_name,
    t('Are you sure you want to delete the St Lucia entity type %entity_label?<br />Deleting the entity type will also delete any Drupal field data you have entered on the entities.<br />This action cannot be undone.', array('%entity_label' => $entity_type->label)),
    t('Delete'),
    t('Cancel')
  );
}

function stlucia_entity_delete_form_submit($form, &$form_state) {
  $entity_type = (object) $form_state['values']['entity_type'];

  // Delete the entity type.
  stlucia_entity_type_delete($entity_type->machine_name);

  // Rebuild the entity info cache and the menus, so that tabs provided by the
  // Field UI module disappear immediately.
  entity_info_cache_clear();
  menu_rebuild();

  // Tell the user what just happened.
  drupal_set_message(t('Deleted the Sarnia entity type %entity_name.', array('%entity_name' => $entity_type->label)));
  $form_state['redirect'] = 'admin/structure/stlucia';
}

<?php

/**
 * @file
 * St Lucia module administration form.
 */

/**
 * Displays the St Lucia entity type admin overview page.
 */
function stlucia_overview_entity_types() {
  $types = stlucia_entity_types();
  // $names = node_type_get_names();
  $field_ui = module_exists('field_ui');
  $header = array(t('Name'), array('data' => t('Operations'), 'colspan' => $field_ui ? '4' : '2'));
  $rows = array();

  foreach ($types as $key => $type) {
    $type_url_str = str_replace('_', '-', $type->machine_name);
    $row = array(theme('stlucia_admin_overview', array('name' => $type->label, 'type' => $type)));
    // Set the edit column.
    $row[] = array('data' => l(t('edit'), 'admin/structure/stlucia/manage/' . $type_url_str));

    if ($field_ui) {
      // Manage fields.
      $row[] = array('data' => l(t('manage fields'), 'admin/structure/stlucia/manage/' . $type_url_str . '/fields'));

      // Display fields.
      $row[] = array('data' => l(t('manage display'), 'admin/structure/stlucia/manage/' . $type_url_str . '/display'));
    }

    // Set the delete column.
    $row[] = array('data' => l(t('delete'), 'admin/structure/stlucia/manage/' . $type_url_str . '/delete'));

    $rows[] = $row;
  }

  $build['node_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No St Lucia entity types available. <a href="@link">Add entity type</a>.', array('@link' => url('admin/structure/stlucia/add'))),
  );

  return $build;
}

/**
 * Returns HTML for a entity type description for the St Lucia entity type
 * admin overview page.
 *
 * @param $variables
 *   An associative array containing:
 *   - name: The human-readable name of the entity type.
 *   - type: An object containing the 'machine_name' and 'description' of
 *     the entity type.
 *
 * @ingroup themeable
 */
function theme_stlucia_admin_overview($variables) {
  $name = $variables['name'];
  $type = $variables['type'];

  $output = check_plain($name);
  $output .= ' <small>' . t('(Machine name: @type)', array('@type' => $type->machine_name)) . '</small>';
  $output .= '<div class="description">' . filter_xss_admin($type->description) . '</div>';
  return $output;
}
